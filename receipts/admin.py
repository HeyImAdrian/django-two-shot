from django.contrib import admin
from .models import Account, Receipt, ExpenseCategory

admin.site.register(Account)
admin.site.register(ExpenseCategory)
admin.site.register(Receipt)
